## Description
Samply is a fast, lightweight, and customizable C++ library for sampling high 
dimensional spaces based on Markov chain Monte Carlo (MCMC). The library uses 
template metaprogramming to simplify the definition of custom random walks, 
sampling spaces and probability distributions.

## License
Samply is freely available under the GNU General Public License v3.
See `LICENSE` or http://www.gnu.org/licenses/ for further information.