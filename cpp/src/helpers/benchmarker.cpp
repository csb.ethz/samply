// Copyright(C) 2018 Mattia Gollub, mattia.gollub@bsse.ethz.ch
// Computational Systems Biology group, ETH Zurich
//
// Based on the original version on: https://github.com/ethz-asl/laser_slam
//
// This software is freely available under the GNU General Public License v3.
// See the LICENSE file or http://www.gnu.org/licenses/ for further information.

#include "helpers/benchmarker.h"

#include <algorithm>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>

namespace samply {

//=================================================================================================
//    Benchmarker implementation
//=================================================================================================

#define ALIGN_MODIFIERS std::setw(70) << std::setfill(' ') << std::left
#define FLOAT_MODIFIERS std::fixed << std::setprecision(2)

void Benchmarker::notifyNewStepStart()
{
    current_timestamp_ = Clock::now();
    ++current_step_id_;
}

void Benchmarker::startMeasurement(const std::string& topic_name)
{
    std::lock_guard<std::mutex> lock(started_measurements_mutex_);
    auto result = started_mesurements_.emplace(topic_name, TimePoint());
    if (result.second == false) {
        std::cout << "WARNING: Starting a measurement for topic '" << topic_name
                  << "' twice.";
    }
    result.first->second = Clock::now();
}

void Benchmarker::stopMeasurement(const std::string& topic_name,
                                  const bool ignore_measurement)
{
    const TimePoint end = Clock::now();
    std::lock_guard<std::mutex> lock(started_measurements_mutex_);
    const auto start_it = started_mesurements_.find(topic_name);
    if (start_it != started_mesurements_.end()) {
        if (!ignore_measurement) {
            addMeasurement(topic_name, start_it->second, end);
        }
        started_mesurements_.erase(start_it);
    } else {
        std::cout << "WARNING: Trying to finish a measurement for topic '" << topic_name
                  << "' which has not been started.";
    }
}

void Benchmarker::addMeasurement(const std::string& topic_name,
                                 const TimePoint start,
                                 const TimePoint end)
{
    double milliseconds = durationToMilliseconds(end - start);

    if (params_.enable_live_output) {
        std::cout << topic_name << " took " << FLOAT_MODIFIERS << milliseconds << "ms."
                  << std::endl;
    }

    std::lock_guard<std::mutex> lock(value_topics_mutex_);
    value_topics_["Times." + topic_name].addValue(current_step_id_, current_timestamp_,
                                                  milliseconds);
}

void Benchmarker::addValue(const std::string& topic_name, const double value)
{
    std::lock_guard<std::mutex> lock(value_topics_mutex_);
    value_topics_["Values." + topic_name].addValue(current_step_id_, current_timestamp_,
                                                   value);
}

void Benchmarker::resetTopic(const std::string& topic_prefix)
{
    std::lock_guard<std::mutex> lock(value_topics_mutex_);
    for (auto topic_it = value_topics_.begin(); topic_it != value_topics_.end();) {
        // Delete topics that have the specified prefix.
        if (topic_it->first.find("Times." + topic_prefix) == 0 ||
            topic_it->first.find("Values." + topic_prefix) == 0) {
            value_topics_.erase(topic_it++);
        } else {
            ++topic_it;
        }
    }

    if (topic_prefix == "") {
        current_timestamp_ = Clock::now();
        current_step_id_ = 0u;
    }
}

void Benchmarker::saveData()
{
    // Visit https://github.com/ethz-asl/laser_slam for the original
    // implementation. Will require Boost.
    throw std::runtime_error("Not implemented");
}

void Benchmarker::logStatistics(std::ostream& stream)
{
    std::lock_guard<std::mutex> lock(value_topics_mutex_);
    stream << std::endl;
    stream << "Statistics:" << std::endl;
    stream << " " << ALIGN_MODIFIERS << "Topic: "
           << "Mean (SD)" << std::endl;

    for (const auto& topic : value_topics_) {
        stream << " " << ALIGN_MODIFIERS << (topic.first + ": ") << FLOAT_MODIFIERS
               << topic.second.getMean() << " (" << FLOAT_MODIFIERS
               << topic.second.getStandardDeviation() << ")" << std::endl;
    }
    stream << "" << std::endl;
}

std::string Benchmarker::setupAndGetResultsRootDirectory()
{
    // Visit https://github.com/ethz-asl/laser_slam for the original
    // implementation. Will require Boost.
    throw std::runtime_error("Not implemented");
}

Benchmarker::TimePoint Benchmarker::getFirstValueTimepoint()
{
    TimePoint first_timepoint = TimePoint::max();
    for (const auto& topic : value_topics_) {
        if (!topic.second.getValues().empty()) {
            first_timepoint =
                std::min(first_timepoint, topic.second.getValues().front().timestamp);
        }
    }
    return first_timepoint;
}

double Benchmarker::durationToMilliseconds(const Duration duration)
{
    constexpr double mus_to_ms = 1.0 / 1000.0;
    auto microseconds =
        std::chrono::duration_cast<std::chrono::microseconds>(duration).count();
    return static_cast<double>(microseconds) * mus_to_ms;
}

//=================================================================================================
//    TimerTopic implementation
//=================================================================================================

void Benchmarker::ValueTopic::addValue(const size_t step_id,
                                       const TimePoint timestamp,
                                       const double value)
{
    sum_ += value;
    sum_of_squares_ += value * value;
    values_count_++;
    if (!Benchmarker::getParameters().save_statistics_only) {
        values_.emplace_back(step_id, timestamp, value);
    }
}

double Benchmarker::ValueTopic::getMean() const
{
    return sum_ / static_cast<double>(values_count_);
}

double Benchmarker::ValueTopic::getStandardDeviation() const
{
    return sqrt(sum_of_squares_ / static_cast<double>(values_count_) -
                pow(sum_ / static_cast<double>(values_count_), 2.0));
}

//=================================================================================================
//    Benchmarker fields
//=================================================================================================

std::mutex Benchmarker::value_topics_mutex_;
std::mutex Benchmarker::started_measurements_mutex_;
std::map<std::string, Benchmarker::ValueTopic> Benchmarker::value_topics_;
std::unordered_map<std::string, Benchmarker::TimePoint>
    Benchmarker::started_mesurements_;
Benchmarker::TimePoint Benchmarker::current_timestamp_;
size_t Benchmarker::current_step_id_ = 0u;
BenchmarkerParams Benchmarker::params_;

//=================================================================================================
//    ScopedTimer implementation
//=================================================================================================

ScopedTimer::~ScopedTimer()
{
    Benchmarker::addMeasurement(topic_name_, start_, Benchmarker::Clock::now());
}

}  // namespace samply
