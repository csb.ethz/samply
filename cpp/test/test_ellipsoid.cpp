// Copyright (C) 2018 Mattia Gollub, mattia.gollub@bsse.ethz.ch
// Computational Systems Biology group, ETH Zurich
//
// This software is freely available under the GNU General Public License v3.
// See the LICENSE file or http://www.gnu.org/licenses/ for further information.

#include <gtest/gtest.h>

#include "descriptors/ellipsoid.h"

using namespace samply;

typedef double Scalar;

TEST(EllipsoidTest, test_ellipsoid_reparametrization)
{
    // Arrange.
    // Use a n-dimensional hypersphere of radius 2 centered at [1 1 ... 1].
    Eigen::Index num_dims = 10;
    Matrix<Scalar> E = Matrix<Scalar>::Identity(num_dims, num_dims) * 4;
    Matrix<Scalar> f = Matrix<Scalar>::Ones(num_dims, 1);
    const Ellipsoid<Scalar> hypersphere(E, f);

    // Act.
    ReparametrizedObject<Ellipsoid<Scalar>> isotropy_hypersphere =
        hypersphere.get_optimally_reparametrized_descriptor("isotropy");
    ReparametrizedObject<Ellipsoid<Scalar>> intersection_hypersphere =
        hypersphere.get_optimally_reparametrized_descriptor("intersection");
    EXPECT_THROW(hypersphere.get_optimally_reparametrized_descriptor("unsupported"),
                 std::runtime_error);

    // Assert.
    EXPECT_TRUE(isotropy_hypersphere.is_standard());
    EXPECT_TRUE(intersection_hypersphere.is_standard());

    EXPECT_TRUE(isotropy_hypersphere.from_frame().get_linear().isApprox(E));
    EXPECT_TRUE(isotropy_hypersphere.from_frame().get_shift().isApprox(f));

    AffineTransform<Scalar> isotropy_identity =
        isotropy_hypersphere.from_frame() * isotropy_hypersphere.to_frame();
    AffineTransform<Scalar> intersection_identity =
        intersection_hypersphere.from_frame() * intersection_hypersphere.to_frame();
    EXPECT_TRUE(isotropy_identity.get_linear().isIdentity());
    EXPECT_TRUE(isotropy_identity.get_shift().isZero());
    EXPECT_TRUE(intersection_identity.get_linear().isIdentity());
    EXPECT_TRUE(intersection_identity.get_shift().isZero());
}

TEST(EllipsoidTest, test_ellipsoid_intersection)
{
    // Arrange.
    // Use a n-dimensional hypersphere of radius 2 centered at [1 1 ... 1].
    Eigen::Index num_dims = 10;
    Matrix<Scalar> E = Matrix<Scalar>::Identity(num_dims, num_dims) * 2;
    Matrix<Scalar> f = Matrix<Scalar>::Ones(num_dims, 1);
    const Ellipsoid<Scalar> hypersphere(E, f);

    // Create test rays.
    RaysPacket<Scalar> rays{Matrix<Scalar>::Ones(num_dims, 2),
                            Matrix<Scalar>::Zero(num_dims, 2)};
    rays.origins(0, 0) = 2;
    rays.origins(1, 1) = 0;
    rays.directions(0, 0) = 1;
    rays.directions(1, 1) = 1;

    // Act.
    IntersectionsPacket<Scalar> intersections = hypersphere.intersect(rays);

    // Assert.
    constexpr double tolerance = 1e-7;
    EXPECT_TRUE(intersections.one_ray_one_intersection);

    EXPECT_NEAR(intersections.segment_starts(0), -3, tolerance);
    EXPECT_NEAR(intersections.segment_ends(0), 1, tolerance);

    EXPECT_NEAR(intersections.segment_starts(1), -1, tolerance);
    EXPECT_NEAR(intersections.segment_ends(1), 3, tolerance);
}

TEST(EllipsoidTest, test_ellipsoid_reparametrized_intersection)
{
    // Arrange.
    // Use a n-dimensional hypersphere of radius 2 centered at [1 1 ... 1].
    Eigen::Index num_dims = 10;
    Matrix<Scalar> E = Matrix<Scalar>::Identity(num_dims, num_dims) * 2;
    Matrix<Scalar> f = Matrix<Scalar>::Ones(num_dims, 1);
    const Ellipsoid<Scalar> hypersphere(E, f);
    const ReparametrizedObject<Ellipsoid<Scalar>> reparametrized_hypersphere =
        hypersphere.get_optimally_reparametrized_descriptor("intersection");

    // Create test rays.
    RaysPacket<Scalar> rays{Matrix<Scalar>::Ones(num_dims, 2),
                            Matrix<Scalar>::Zero(num_dims, 2)};
    rays.origins(0, 0) = 2;
    rays.origins(1, 1) = 0;
    rays.directions(0, 0) = 1;
    rays.directions(1, 1) = 1;

    rays = reparametrized_hypersphere.to_frame() * rays;

    // Act.
    IntersectionsPacket<Scalar> intersections =
        reparametrized_hypersphere.intersect(rays);

    // Assert.
    constexpr double tolerance = 1e-7;
    EXPECT_TRUE(intersections.one_ray_one_intersection);

    EXPECT_NEAR(intersections.segment_starts(0), -3, tolerance);
    EXPECT_NEAR(intersections.segment_ends(0), 1, tolerance);

    EXPECT_NEAR(intersections.segment_starts(1), -1, tolerance);
    EXPECT_NEAR(intersections.segment_ends(1), 3, tolerance);
}