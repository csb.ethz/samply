// Copyright (C) 2018 Mattia Gollub, mattia.gollub@bsse.ethz.ch
// Computational Systems Biology group, ETH Zurich
//
// This software is freely available under the GNU General Public License v3.
// See the LICENSE file or http://www.gnu.org/licenses/ for further information.

#include <commons.h>
#include <gtest/gtest.h>
#include <transforms/transforms.h>

using namespace samply;

typedef double Scalar;

void transform_and_verify_matrix(const AffineTransform<Scalar>& transform,
                                 const Matrix<Scalar>& matrix,
                                 const Matrix<Scalar>& expected_result)
{
    Matrix<Scalar> transformed_matrix = transform * matrix;
    EXPECT_TRUE(transformed_matrix.isApprox(expected_result));
}

void transform_and_verify_transform(const AffineTransform<Scalar>& transform_1,
                                    const AffineTransform<Scalar>& transform_2,
                                    const AffineTransform<Scalar>& expected_result)
{
    AffineTransform<Scalar> transformed_transform = transform_1 * transform_2;
    EXPECT_TRUE(
        transformed_transform.get_linear().isApprox(expected_result.get_linear()));
    EXPECT_TRUE(
        transformed_transform.get_shift().isApprox(expected_result.get_shift()));
    EXPECT_TRUE(
        transformed_transform.get_diagonal().isApprox(expected_result.get_diagonal()));
}

//==============================================================================
//	Test dense affine transforms.
//==============================================================================

TEST(TransformsTest, test_dense_transform_matrix)
{
    // Arrange.
    // Create affine transform.
    AffineTransform<Scalar> transform(
        (Matrix<Scalar>(2, 3) << -3, 2, 1, 1, 0, -1).finished(),
        (Vector<Scalar>(2) << 1, 0).finished());

    // Create test matrix.
    Matrix<Scalar> matrix(3, 2);
    matrix << 2, -1, 1, 0, -1, 2;

    // Create expected results.
    Matrix<Scalar> expected_transformed_matrix(2, 2);
    expected_transformed_matrix << -4, 6, 3, -3;

    // Act and assert.
    transform_and_verify_matrix(transform, matrix, expected_transformed_matrix);
}

TEST(TransformsTest, test_dense_transform_transform)
{
    // Arrange.
    // Create affine transform.
    AffineTransform<Scalar> transform_1(
        (Matrix<Scalar>(2, 3) << -3, 2, 1, 1, 0, -1).finished(),
        (Vector<Scalar>(2) << 1, 0).finished());

    // Create test transform.
    AffineTransform<Scalar> transform_2(
        (Matrix<Scalar>(3, 2) << 2, -1, 1, 0, -1, 2).finished(),
        (Vector<Scalar>(3) << 1, 0, -2).finished());

    // Create expected results.
    AffineTransform<Scalar> transformed_transform(
        (Matrix<Scalar>(2, 2) << -5, 5, 3, -3).finished(),
        (Vector<Scalar>(2) << -4, 3).finished());

    // Act and assert.
    transform_and_verify_transform(transform_1, transform_2, transformed_transform);
}

//==============================================================================
//	Test dense linear transforms.
//==============================================================================

TEST(TransformsTest, test_linear_transform_matrix)
{
    // Arrange.
    // Create affine transform.
    AffineTransform<Scalar> transform(
        (Matrix<Scalar>(2, 3) << -3, 2, 1, 1, 0, -1).finished(),
        (Vector<Scalar>(2) << 0, 0).finished());

    // Create test matrix.
    Matrix<Scalar> matrix(3, 2);
    matrix << 2, -1, 1, 0, -1, 2;

    // Create expected results.
    Matrix<Scalar> expected_transformed_matrix(2, 2);
    expected_transformed_matrix << -5, 5, 3, -3;

    // Act and assert.
    transform_and_verify_matrix(transform, matrix, expected_transformed_matrix);
}

TEST(TransformsTest, test_linear_transform_transform)
{
    // Arrange.
    // Create affine transform.
    AffineTransform<Scalar> transform_1(
        (Matrix<Scalar>(2, 3) << -3, 2, 1, 1, 0, -1).finished(),
        (Vector<Scalar>(2) << 0, 0).finished());

    // Create test transform.
    AffineTransform<Scalar> transform_2(
        (Matrix<Scalar>(3, 2) << 2, -1, 1, 0, -1, 2).finished(),
        (Vector<Scalar>(3) << 1, 0, -2).finished());

    // Create expected results.
    AffineTransform<Scalar> transformed_transform(
        (Matrix<Scalar>(2, 2) << -5, 5, 3, -3).finished(),
        (Vector<Scalar>(2) << -5, 3).finished());

    // Act and assert.
    transform_and_verify_transform(transform_1, transform_2, transformed_transform);
}

//==============================================================================
//	Test diagonal affine transforms.
//==============================================================================

TEST(TransformsTest, test_diagonal_affine_transform_matrix)
{
    // Arrange.
    // Create affine transform.
    AffineTransform<Scalar> transform(
        (Matrix<Scalar>(2, 3) << -3, 0, 0, 0, 2, 0).finished(),
        (Vector<Scalar>(2) << 1, 0).finished());

    // Create test matrix.
    Matrix<Scalar> matrix(3, 2);
    matrix << 2, -1, 1, 0, -1, 2;

    // Create expected results.
    Matrix<Scalar> expected_transformed_matrix(2, 2);
    expected_transformed_matrix << -5, 4, 2, 0;

    // Act and assert.
    transform_and_verify_matrix(transform, matrix, expected_transformed_matrix);
}

TEST(TransformsTest, test_diagonal_affine_transform_transform)
{
    // Arrange.
    // Create affine transform.
    AffineTransform<Scalar> transform_1(
        (Matrix<Scalar>(2, 3) << -3, 0, 0, 0, 2, 0).finished(),
        (Vector<Scalar>(2) << 1, 0).finished());

    // Create test transform.
    AffineTransform<Scalar> transform_2(
        (Matrix<Scalar>(3, 2) << 2, -1, 1, 0, -1, 2).finished(),
        (Vector<Scalar>(3) << 1, 0, -2).finished());

    // Create expected results.
    AffineTransform<Scalar> transformed_transform(
        (Matrix<Scalar>(2, 2) << -6, 3, 2, 0).finished(),
        (Vector<Scalar>(2) << -2, 0).finished());

    // Act and assert.
    transform_and_verify_transform(transform_1, transform_2, transformed_transform);
}

//==============================================================================
//	Test diagonal linear transforms.
//==============================================================================

TEST(TransformsTest, test_diagonal_linear_transform_matrix)
{
    // Arrange.
    // Create affine transform.
    AffineTransform<Scalar> transform(
        (Matrix<Scalar>(2, 3) << -3, 0, 0, 0, 2, 0).finished(),
        (Vector<Scalar>(2) << 0, 0).finished());

    // Create test matrix.
    Matrix<Scalar> matrix(3, 2);
    matrix << 2, -1, 1, 0, -1, 2;

    // Create expected results.
    Matrix<Scalar> expected_transformed_matrix(2, 2);
    expected_transformed_matrix << -6, 3, 2, 0;

    // Act and assert.
    transform_and_verify_matrix(transform, matrix, expected_transformed_matrix);
}

TEST(TransformsTest, test_diagonal_linear_transform_transform)
{
    // Arrange.
    // Create affine transform.
    AffineTransform<Scalar> transform_1(
        (Matrix<Scalar>(2, 3) << -3, 0, 0, 0, 2, 0).finished(),
        (Vector<Scalar>(2) << 0, 0).finished());

    // Create test transform.
    AffineTransform<Scalar> transform_2(
        (Matrix<Scalar>(3, 2) << 2, -1, 1, 0, -1, 2).finished(),
        (Vector<Scalar>(3) << 1, 0, -2).finished());

    // Create expected results.
    AffineTransform<Scalar> transformed_transform(
        (Matrix<Scalar>(2, 2) << -6, 3, 2, 0).finished(),
        (Vector<Scalar>(2) << -3, 0).finished());

    // Act and assert.
    transform_and_verify_transform(transform_1, transform_2, transformed_transform);
}

//==============================================================================
//	Test translation transforms.
//==============================================================================

TEST(TransformsTest, test_translation_transform_matrix)
{
    // Arrange.
    // Create affine transform.
    AffineTransform<Scalar> transform(
        (Matrix<Scalar>(2, 3) << 1, 0, 0, 0, 1, 0).finished(),
        (Vector<Scalar>(2) << 1, 0).finished());

    // Create test matrix.
    Matrix<Scalar> matrix(3, 2);
    matrix << 2, -1, 1, 0, -1, 2;

    // Create expected results.
    Matrix<Scalar> expected_transformed_matrix(2, 2);
    expected_transformed_matrix << 3, 0, 1, 0;

    // Act and assert.
    transform_and_verify_matrix(transform, matrix, expected_transformed_matrix);
}

TEST(TransformsTest, test_translation_transform_transform)
{
    // Arrange.
    // Create affine transform.
    AffineTransform<Scalar> transform_1(
        (Matrix<Scalar>(2, 3) << 1, 0, 0, 0, 1, 0).finished(),
        (Vector<Scalar>(2) << 1, 0).finished());

    // Create test transform.
    AffineTransform<Scalar> transform_2(
        (Matrix<Scalar>(3, 2) << 2, -1, 1, 0, -1, 2).finished(),
        (Vector<Scalar>(3) << 1, 0, -2).finished());

    // Create expected results.
    AffineTransform<Scalar> transformed_transform(
        (Matrix<Scalar>(2, 2) << 2, -1, 1, 0).finished(),
        (Vector<Scalar>(2) << 2, 0).finished());

    // Act and assert.
    transform_and_verify_transform(transform_1, transform_2, transformed_transform);
}

//==============================================================================
//	Test translation transforms.
//==============================================================================

TEST(TransformsTest, test_identity_transform_matrix)
{
    // Arrange.
    // Create affine transform.
    AffineTransform<Scalar> transform(
        (Matrix<Scalar>(2, 3) << 1, 0, 0, 0, 1, 0).finished(),
        (Vector<Scalar>(2) << 0, 0).finished());

    // Create test matrix.
    Matrix<Scalar> matrix(3, 2);
    matrix << 2, -1, 1, 0, -1, 2;

    // Create expected results.
    Matrix<Scalar> expected_transformed_matrix(2, 2);
    expected_transformed_matrix << 2, -1, 1, 0;

    // Act and assert.
    transform_and_verify_matrix(transform, matrix, expected_transformed_matrix);
}

TEST(TransformsTest, test_identity_transform_transform)
{
    // Arrange.
    // Create affine transform.
    AffineTransform<Scalar> transform_1(
        (Matrix<Scalar>(2, 3) << 1, 0, 0, 0, 1, 0).finished(),
        (Vector<Scalar>(2) << 0, 0).finished());

    // Create test transform.
    AffineTransform<Scalar> transform_2(
        (Matrix<Scalar>(3, 2) << 2, -1, 1, 0, -1, 2).finished(),
        (Vector<Scalar>(3) << 1, 0, -2).finished());

    // Create expected results.
    AffineTransform<Scalar> transformed_transform(
        (Matrix<Scalar>(2, 2) << 2, -1, 1, 0).finished(),
        (Vector<Scalar>(2) << 1, 0).finished());

    // Act and assert.
    transform_and_verify_transform(transform_1, transform_2, transformed_transform);
}

TEST(TransformsTest, test_inverse)
{
    // Arrange.
    // Create affine transforms.
    Matrix<Scalar> T(2, 3);
    Vector<Scalar> shift(2);
    T << 1, 2, 0, -2, -4, 6;
    shift << 1, -1;
    AffineTransform<Scalar> affine(T, shift);
    IdentityTransform<Scalar> identity(2);

    // Act.
    auto inverse_affine = affine.inverse();
    auto inverse_identity = identity.inverse();

    auto affine_times_inverse_affine = affine * inverse_affine;
    auto identity_times_inverse_identity = identity * inverse_identity;

    // Assert.
    EXPECT_TRUE(affine_times_inverse_affine.get_linear().isIdentity());
    EXPECT_TRUE(affine_times_inverse_affine.get_shift().isZero());
    EXPECT_TRUE(identity_times_inverse_identity.get_linear().isIdentity());
    EXPECT_TRUE(identity_times_inverse_identity.get_shift().isZero());
}