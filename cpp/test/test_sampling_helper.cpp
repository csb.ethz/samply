// Copyright (C) 2018 Mattia Gollub, mattia.gollub@bsse.ethz.ch
// Computational Systems Biology group, ETH Zurich
//
// This software is freely available under the GNU General Public License v3.
// See the LICENSE file or http://www.gnu.org/licenses/ for further information.

#include <gtest/gtest.h>

#include "../include/helpers/sampling_helper.h"

using namespace samply;

typedef double Scalar;

TEST(SamplingHelperTest, test_random_uniform_scalar_bounds)
{
    // Arrange.
    SamplingHelper helper;
    constexpr int num_rows = 50;
    constexpr int num_cols = 100;
    constexpr Scalar lower_bound = -5.0;
    constexpr Scalar upper_bound = 20.0;

    // Act.
    Matrix<Scalar> result =
        helper.get_random_uniform(lower_bound, upper_bound, num_rows, num_cols);

    // Assert.
    // Note that this only performs very superficial checks and doesn't actually
    // verify the correctness of the distribution.
    for (Eigen::Index i = 0; i < result.size(); ++i) {
        EXPECT_LE(lower_bound, result(i));
        EXPECT_GE(upper_bound, result(i));
    }
}

TEST(SamplingHelperTest, test_random_uniform_matrix_bounds)
{
    // Arrange.
    SamplingHelper helper;
    constexpr int num_rows = 50;
    constexpr int num_cols = 100;
    const Matrix<Scalar> lower_bounds =
        Matrix<Scalar>::Random(num_rows, num_cols) -
        Matrix<Scalar>::Constant(num_rows, num_cols, 1.1);
    const Matrix<Scalar> upper_bounds =
        Matrix<Scalar>::Random(num_rows, num_cols) +
        Matrix<Scalar>::Constant(num_rows, num_cols, 1.1);

    // Act.
    Matrix<Scalar> result = helper.get_random_uniform(lower_bounds, upper_bounds);

    // Assert.
    // Note that this only performs very superficial checks and doesn't actually
    // verify the correctness of the distribution.
    for (Eigen::Index i = 0; i < result.size(); ++i) {
        EXPECT_LE(lower_bounds(i), result(i));
        EXPECT_GE(upper_bounds(i), result(i));
    }
}

TEST(SamplingHelperTest, test_random_normal)
{
    // Arrange.
    SamplingHelper helper;
    constexpr int num_rows = 50;
    constexpr int num_cols = 100;
    typedef Eigen::Matrix<Scalar, num_rows, num_cols> TestMatrix;
    constexpr Scalar mean = 5.0;
    constexpr Scalar std_dev = 2.0;

    // Act.
    TestMatrix result =
        helper.get_random_normals(mean, std_dev, num_rows, num_cols).eval();

    // Assert.
    // Note that this only performs very superficial checks and doesn't actually
    // verify the correctness of the distribution.
    const Scalar average = result.mean();
    EXPECT_LE(mean - std_dev / 2.0, average);
    EXPECT_GE(mean + std_dev / 2.0, average);
}

TEST(SamplingHelperTest, test_random_directions)
{
    // Arrange.
    SamplingHelper helper;
    constexpr int num_dims = 50;
    constexpr int num_dirs = 100;
    constexpr Scalar tolerance = 0.001;
    typedef Eigen::Matrix<Scalar, num_dims, num_dirs> TestMatrix;

    // Act.
    TestMatrix result = helper.get_random_directions<Scalar>(num_dims, num_dirs).eval();

    // Assert.
    // Note that this only performs very superficial checks and doesn't actually
    // verify the correctness of the distribution.
    for (Eigen::Index i = 0; i < result.cols(); ++i) {
        const Scalar norm = result.col(i).norm();
        EXPECT_LE(1.0 - tolerance, norm);
        EXPECT_GE(1.0 + tolerance, norm);
    }
    Scalar total_displacement = result.colwise().sum().norm();
    EXPECT_GE(std::sqrt(static_cast<Scalar>(num_dirs * num_dims)), total_displacement);
}