// Copyright (C) 2018 Mattia Gollub, mattia.gollub@bsse.ethz.ch
// Computational Systems Biology group, ETH Zurich
//
// This software is freely available under the GNU General Public License v3.
// See the LICENSE file or http://www.gnu.org/licenses/ for further information.

#include <gtest/gtest.h>

#include "chord_samplers/mvn_pdf_sampler.h"
#include "helpers/sampling_helper.h"

using namespace samply;

typedef double Scalar;

TEST(MvnPdfSamplerTest, test_is_point_on_segment)
{
    // Arrange.
    SamplingHelper helper;

    Matrix<Scalar> mvn_T(3, 3);
    Vector<Scalar> mvn_mean(3);
    mvn_T << -6.4699, 2.7738, -4.2843, -6.4699, -5.5476, -0.0000, -6.4699, 2.7738,
        4.2843;
    mvn_mean << 2.75, 3, 3.25;

    MvnPdfSampler<Scalar> sampler(
        AffineTransform<Scalar>((Matrix<Scalar>(3, 3) << -6.4699, 2.7738, -4.2843,
                                 -6.4699, -5.5476, -0.0000, -6.4699, 2.7738, 4.2843)
                                    .finished(),
                                (Vector<Scalar>(3) << 2.75, 3, 3.25).finished()));
    auto sampler_Fs = sampler.get_optimally_reparametrized_sampler();

    RaysPacket<Scalar> rays{
        (Matrix<Scalar>(3, 4) << 5, 8, 3, 9, 1, 6, 2, 7, 3, 6, 2, 7).finished(),
        (Matrix<Scalar>(3, 4) << 1, 3, 6, 2, 2, 6, 3, 7, 3, 1, 2, 1)
            .finished()
            .colwise()
            .normalized()};

    IntersectionsPacket<Scalar> intersections(
        (Vector<Scalar>(4) << -1, -2, -5, -3).finished(),
        (Vector<Scalar>(4) << 4, 1, 3, 5).finished());

    // Act.
    Vector<Scalar> t_samples =
        sampler_Fs.sample1d(sampler_Fs.to_frame() * rays, intersections, helper);

    // Assert.
    for (Eigen::Index i = 0; i < t_samples.cols(); ++i) {
        // Verify that the sampled point lies within the bounds of the chord.
        EXPECT_LE(intersections.segment_starts(i), t_samples[i]);
        EXPECT_GE(intersections.segment_ends(i), t_samples[i]);
    }
}

TEST(MvnPdfSamplerTest, test_is_point_on_segments_union)
{
    // Arrange.
    SamplingHelper helper;

    Matrix<Scalar> mvn_T(3, 3);
    Vector<Scalar> mvn_mean(3);
    mvn_T << -6.4699, 2.7738, -4.2843, -6.4699, -5.5476, -0.0000, -6.4699, 2.7738,
        4.2843;
    mvn_mean << 2.75, 3, 3.25;

    MvnPdfSampler<Scalar> sampler(
        AffineTransform<Scalar>((Matrix<Scalar>(3, 3) << -6.4699, 2.7738, -4.2843,
                                 -6.4699, -5.5476, -0.0000, -6.4699, 2.7738, 4.2843)
                                    .finished(),
                                (Vector<Scalar>(3) << 2.75, 3, 3.25).finished()));
    auto sampler_Fs = sampler.get_optimally_reparametrized_sampler();

    RaysPacket<Scalar> rays{
        (Matrix<Scalar>(3, 4) << 5, 8, 3, 9, 1, 6, 2, 7, 3, 6, 2, 7).finished(),
        (Matrix<Scalar>(3, 4) << 1, 3, 6, 2, 2, 6, 3, 7, 3, 1, 2, 1)
            .finished()
            .colwise()
            .normalized()};

    IntersectionsPacket<Scalar> intersections(
        (Eigen::VectorXd(8) << -1, -5, -2, -9, -5, 6, -3, 8).finished(),
        (Eigen::VectorXd(8) << 4, -3, 1, -4, 3, 7, 5, 9).finished(),
        (Eigen::VectorXi(8) << 0, 0, 1, 1, 2, 2, 3, 3).finished(), 4);

    // Act.
    Vector<Scalar> t_samples =
        sampler_Fs.sample1d(sampler_Fs.to_frame() * rays, intersections, helper);

    // Assert.
    for (Eigen::Index i = 0; i < t_samples.cols(); ++i) {
        EXPECT_TRUE((intersections.segment_starts(2 * i) <= t_samples[i] &&
                     t_samples[i] <= intersections.segment_ends(2 * i)) ||
                    (intersections.segment_starts(2 * i + 1) <= t_samples[i] &&
                     t_samples[i] <= intersections.segment_ends(2 * i + 1)));
    }
}