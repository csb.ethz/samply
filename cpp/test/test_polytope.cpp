// Copyright (C) 2018 Mattia Gollub, mattia.gollub@bsse.ethz.ch
// Computational Systems Biology group, ETH Zurich
//
// This software is freely available under the GNU General Public License v3.
// See the LICENSE file or http://www.gnu.org/licenses/ for further information.

#include <gtest/gtest.h>

#include "../include/descriptors/polytope.h"
#include "../include/helpers/sampling_helper.h"

using namespace samply;

typedef double Scalar;

TEST(PolytopeTest, test_polytope_intersection)
{
    // Arrange.
    // Use a n-dimensional hypercube defined by the corners:
    // [-1, -1, ..., -1] and [1, 1, ..., 1]
    constexpr int num_dims = 4;

    Polytope<Scalar> hypercube(
        (Matrix<Scalar>(8, 4) << 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, -1, 0,
         0, 0, 0, -1, 0, 0, 0, 0, -1, 0, 0, 0, 0, -1)
            .finished(),
        Vector<Scalar>::Ones(8));

    RaysPacket<Scalar> rays{
        (Matrix<Scalar>(4, 2) << 0, 0.5, 0, 0, 0, 0, 0, 0).finished(),
        (Matrix<Scalar>(4, 2) << 1, 1, 1, 0, 1, 0, 1, 0)
            .finished()
            .colwise()
            .normalized()};
    hypercube.initialize(rays.origins);

    // Act.
    IntersectionsPacket<Scalar> result = hypercube.intersect(rays);

    // Assert.
    constexpr double tolerance = 1e-6;
    EXPECT_NEAR(result.segment_starts(0), -std::sqrt(num_dims), tolerance);
    EXPECT_NEAR(result.segment_ends(0), std::sqrt(num_dims), tolerance);
    EXPECT_NEAR(result.segment_starts(1), -1.5, tolerance);
    EXPECT_NEAR(result.segment_ends(1), 0.5, tolerance);
}